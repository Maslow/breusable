﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Utilities;
using MSBuild.Community.Tasks.Ftp;
using Microsoft.Build.Framework;

namespace BFtp
{
	public abstract class BFtpTaskHelper:FtpClientTaskBase
	{
		
		internal  bool ConnectAndProcess(Action connectedProcessAction)
		{
			try
			{
				try
				{
					Connect();
					Log.LogMessage(MessageImportance.Low, "Connected to remote server.");
				}
				catch (FtpException caught)
				{
					Log.LogErrorFromException(caught, false);
					Log.LogError("Could not connect to remote server. {0}", caught.Message);
					return false;
				}

				try
				{
					Login();
					Log.LogMessage(MessageImportance.Low, "Login succesfully.");
				}
				catch (Exception caught)
				{
					Log.LogErrorFromException(caught, false);
					Log.LogError("Could not login. {0}", caught.Message);
					return false;
				}
				connectedProcessAction();
			}
			finally
			{
				Close();
			}

			Log.LogMessage("Full directory path created remotely.");
			return true;
		}
	}
}
