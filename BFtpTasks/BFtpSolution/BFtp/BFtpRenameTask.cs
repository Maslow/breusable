﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Framework;
using MSBuild.Community.Tasks.Ftp;
using Microsoft.Build.Utilities;

namespace BFtp
{
	public class BFtpRenameTask : BFtpTaskHelper
	{
		private ITaskItem[] _remoteFiles;
		/// <summary>
		/// Gets or sets the remote files to upload.
		/// Each item in this list should have a corresponding item in LocalFiles.
		/// </summary>
		[Required]
		public ITaskItem[] RemoteFiles
		{
			get { return _remoteFiles; }
			set { _remoteFiles = value; }
		}
		private void Processing()
		{
			foreach (var item in RemoteFiles)
			{
				processItem(item);
			}
		}
		public override bool Execute()
		{
			return ConnectAndProcess(Processing);
		}
		public  void processItem(ITaskItem item)
		{
			if (FileExists(item.ItemSpec))
			{
				var reply = SendCommandAndReadResponse("rename " + item.ItemSpec);
				Log.LogMessage("Delete " + item.ItemSpec + " returned " + reply.ResultCode.ToString() + ";"
					+ reply.Message);
			}
			else
				Log.LogWarning("File does not exist:" + item.ItemSpec, null);
		}
		

	}
}
