﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Framework;
using MSBuild.Community.Tasks.Ftp;

namespace BFtp
{
	public class BFtpDeleteTask : BFtpTaskHelper
	{
		private ITaskItem[] _remoteFiles;
		public bool IgnoreFileNotExists { get; private set; }
		/// <summary>
		/// Gets or sets the remote files to upload.
		/// Each item in this list should have a corresponding item in LocalFiles.
		/// </summary>
		[Required]
		public ITaskItem[] RemoteFiles
		{
			get { return _remoteFiles; }
			set { _remoteFiles = value; }
		}
		private void Processing()
		{
			Log.LogMessage("CWD:" + this.GetWorkingDirectory());
			//Log.LogMessage("Contents:" + this.GetDirectoryDetails().Select(c=>c.Name).Aggregate((s1,s2)=>s1+","+s2));
			foreach (var item in RemoteFiles)
			{
				processItem(item);
			}
		}

		public override bool Execute()
		{
			return ConnectAndProcess(Processing);
		}

		public void processItem(ITaskItem item)
		{
			if (item.ItemSpec.EndsWith("/"))
				throw new ArgumentException("Delete Task is for files, can not delete:" + item.ItemSpec);
			//var dirs = item.ItemSpec.Split("/", StringSplitOptions.RemoveEmptyEntries);
			//if (item.ItemSpec.StartsWith(this.GetWorkingDirectory()))
			var targetFile = item.ItemSpec;
			if (item.ItemSpec.Contains("/"))
			{
				ChangeWorkingDirectory(item.ItemSpec.Substring(0, item.ItemSpec.LastIndexOf("/")));
				targetFile = item.ItemSpec.Substring(item.ItemSpec.LastIndexOf("/") + 1);
			}

			Log.LogMessage("Deleting " + targetFile);
			var reply = SendCommandAndReadResponse("dele", targetFile);
			Log.LogMessage("Delete " + item.ItemSpec + " returned " + reply.ResultCode.ToString() + ";"
				+ reply.Message);
			if (reply.ResultCode != 250)
			{
				Log.LogWarning("File does not exist in" + this.GetWorkingDirectory() + ":" + item.ItemSpec, null);
				Log.LogMessage("Contents:" + this.GetDirectoryDetails().Select(c => c.Name).Aggregate((s1, s2) => s1 + "," + s2));
				if (IgnoreFileNotExists == false)
					throw new FtpException("Failed to delete file");
			}



		}

	}
}
