﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BMvcReusable
{
    public class BStrings
    {
        public static string HrefFromString(string location, string text)
        {
            var tagBuilder = new TagBuilder("a");
            tagBuilder.Attributes.Add("href", location);
            tagBuilder.SetInnerText(text);
            return tagBuilder.ToString();
        }
    }
}
