﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BMvcReusable
{
    public static class Extensions
    {

        /// <summary>
        /// BMvcReusable
        /// http://stackoverflow.com/questions/1288046/how-can-i-get-my-webapps-base-url-in-asp-net-mvc
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string AppBase(this UrlHelper helper, HttpRequest request)
        {
            return string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, helper.Content("~"));
        }

        /// <summary>
        /// BMvcReusable
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ToHref(this Uri uri,string text)
        {
            var tagBuilder = new TagBuilder("a");
            tagBuilder.Attributes.Add("href", uri.AbsolutePath);
            tagBuilder.SetInnerText(text);
            return tagBuilder.ToString();
        }

        /// <summary>
        /// BMvcReusable
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <returns></returns>
        public static string ControllerName<TController>()
            where TController : Controller
        {
            var name = typeof(TController).Name;

            return name.EndsWith("Controller")?
                name.Substring(0, name.Length - "Controller".Length)
                :name;
        }

        /// <summary>
        /// BMvcReusable
        /// </summary>
        /// <param name="text"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static TagBuilder BuildTag(this string text, string tag)
        {
            var tagBuilder = new TagBuilder(tag);
            tagBuilder.SetInnerText(text);
            return tagBuilder;
        }

        
        /// <summary>
        /// BMvcReusable
        /// </summary>
        /// <param name="text"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static TagBuilder BuildXmlTag(this string text, string tag)
        {
            return new TagBuilder(tag) { InnerHtml = text };
        }
        /// <summary>
        /// BMvcReusable
        /// </summary>
        /// <param name="tagBuilder"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TagBuilder MergeAttributeFl(this TagBuilder tagBuilder,string key,string value)
        {
            tagBuilder.MergeAttribute(key, value);

            return tagBuilder;
        }
        /// <summary>
        /// BMvcReusable
        /// </summary>
        /// <param name="text"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static string WrapXmlInTag(this string text, string tag)
        {
            var t = new TagBuilder(tag);
            t.InnerHtml = text;
            return t.ToString();
        }

        /// <summary>
        /// BMvcReusable
        /// </summary>
        /// <param name="text"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static string WrapInTag(this string text, string tag)
        {
            return WrapInTag(text, tag, TagRenderMode.Normal);
        }

        public static MvcHtmlString WrapXmlInTag(this MvcHtmlString text, string tag)
        {
            
            return MvcHtmlString.Create(text.ToHtmlString().WrapXmlInTag(tag));
        }

        /// <summary>
        /// BMvcReusable
        /// </summary>
        /// <param name="text"></param>
        /// <param name="tag"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static string WrapInTag(this string text, string tag, TagRenderMode mode)
        {
            var t = new TagBuilder(tag);
            t.SetInnerText(text);
            
            return t.ToString(mode);
        }

        /// <summary>
        /// BMvcReusable
        /// </summary>
        /// <param name="text"></param>
        /// <param name="tag"></param>
        /// <param name="tagAdditions"></param>
        /// <returns></returns>
        public static string WrapInTag(this string text, string tag, Action<TagBuilder> tagAdditions)
        {
            var t = new TagBuilder(tag);
            t.SetInnerText(text);
            tagAdditions(t);
            return t.ToString(TagRenderMode.Normal);
        }

        /// <summary>
        /// BMvcReusable
        /// </summary>
        /// <param name="text"></param>
        /// <param name="tag"></param>
        /// <param name="mode"></param>
        /// <param name="tagAdditions"></param>
        /// <returns></returns>
        public static string WrapInTag(this string text, string tag,TagRenderMode mode, Action<TagBuilder> tagAdditions)
        {
            var t = new TagBuilder(tag);
            t.SetInnerText(text);
            tagAdditions(t);
            return t.ToString(mode);
        }
    }
}
