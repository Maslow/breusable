﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BReusable.StaticReflection;
using System.Diagnostics;

namespace Maybe
{
	class Program
	{
		
		static void Main(string[] args)
		{
			
			Employee emp1 = new Employee()
			{
				Person = new Person()
				{
					PersonAddress = new Address()
					{
						CityId = 1,
						AddrId = 5,
						Addr = "Blanding Blvd.",
						AddrF= "Blanding Str.",
						FieldId=1,
						FieldNId=1
					},
					Address2= new Address()
					{
						CityId = 2,
						AddrId = 6,
						Addr = "Blanding Blvd.2",
						AddrF = "Blanding Str.2",
						FieldId = 2,
						FieldNId =2
					}
				}
			};
			Employee emp2 = new Employee()
			{
				Person=new Person()
			};
			Employee emp3 = new Employee();
			Employee emp4 = null;
			var emps = new Employee[] { emp1, emp2, emp3, emp4 };
			foreach (var emp in emps)
			{
				"Emp:".Dump();
				emp.MaybeDeep(e => e.Person.PersonAddress.Addr).Dump();
				emp.MaybeDeep(e => e.Person.PersonAddress.AddrF).Dump();

				emp.MaybeDeepToNullable(e => e.Person.PersonAddress, pa => pa.AddrId).Dump();
				emp.MaybeDeepToNullable(e => e.Person.PersonAddress, pa => pa.FieldId).Dump();

				emp.MaybeDeep(e => e.Person.PersonAddress.CityId).Dump();
				emp.MaybeDeep(e => e.Person.PersonAddress.FieldNId).Dump();

				"Emp:Address2:".Dump();
				emp.MaybeDeep(e => e.Person.Address2.Addr).Dump();
				emp.MaybeDeep(e => e.Person.Address2.AddrF).Dump();

				emp.MaybeDeepToNullable(e => e.Person.Address2, pa => pa.AddrId).Dump();
				emp.MaybeDeepToNullable(e => e.Person.Address2, pa => pa.FieldId).Dump();

				emp.MaybeDeep(e => e.Person.Address2.CityId).Dump();
				emp.MaybeDeep(e => e.Person.Address2.FieldNId).Dump();

			}
			
			Console.ReadLine();
		}
	}

	public static class Extensions
	{
		public static T Dump<T>(this T t)
		{
			if (t == null)
				Console.WriteLine("null");
			else
			Console.WriteLine(t.ToString());
			return t;
		}
	}
	public class Address
	{
		public int? CityId { get; set; }
		public int AddrId { get; set; }
		public string Addr { get; set; }
		public int FieldId;
		public int? FieldNId;
		public string AddrF;
	}
	public class Person
	{
		public Address PersonAddress { get; set; }
		public Address Address2=null;
	}
	public class Employee
	{
		public Person Person { get; set; }
	}
}
