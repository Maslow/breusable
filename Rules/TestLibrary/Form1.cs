﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestLibrary
{
	public partial class Form1:Form
	{
		public bool test=false;
		internal int internalField;
		public Action act = ( ) => MessageBox.Show("test");

		public Form1( )
		{
			InitializeComponent( );
		}

		public int badfield;

		public Form1(string test)
			: this( )
		{

		}

		public Form1(bool test2)
		{
#if DEBUG
			Debug.WriteLine("In debug!");
#endif
		}
		public Form1(bool test, string testText)
		{

		}
		public Form1(int testInt)
			: this(true)
		{
			throw new NotImplementedException( );
		}
		public Form1(int? testInt)
			: this(false)
		{
			Action init = ( ) => InitializeComponent( );
		}
		public Form1(bool? testBool)
			: this(true)
		{
			{
				InitializeComponent( );
			}
		}

		public Form1(bool? testBool, int? testInt)
		{
			if (false)
				InitializeComponent( );
		}
		/// <summary>
		/// Incorrectly fails the rule
		/// </summary>
		/// <param name="testInt"></param>
		/// <param name="testBool"></param>
		/// <param name="testBool2"></param>
		public Form1(int? testInt, bool testBool, bool testBool2)
			: this(false)
		{
			Action init = ( ) => InitializeComponent( );
			init( );
		}
		/// <summary>
		/// incorrectly fails the rule
		/// </summary>
		/// <param name="testInt"></param>
		/// <param name="testBool"></param>
		/// <param name="testBool2"></param>
		private Form1(int? testInt, bool testBool, bool? testBool2)
			: this(false)
		{
			Action init = InitializeComponent;
			init( );
		}
		private void Form1_Load(object sender, EventArgs e)
		{

		}
	}
}
