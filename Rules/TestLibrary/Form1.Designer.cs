﻿namespace TestLibrary
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing&&(components!=null))
			{
				components.Dispose( );
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent( )
		{
			this.ultraLabel1=new Infragistics.Win.Misc.UltraLabel( );
			this.ultraGroupBox1=new Infragistics.Win.Misc.UltraGroupBox( );
			this.ultraTextEditor1=new Infragistics.Win.UltraWinEditors.UltraTextEditor( );
			this.button1=new System.Windows.Forms.Button( );
			this.label1=new System.Windows.Forms.Label( );
			((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit( );
			((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit( );
			this.SuspendLayout( );
			// 
			// ultraLabel1
			// 
			this.ultraLabel1.Location=new System.Drawing.Point(12, 12);
			this.ultraLabel1.Name="ultraLabel1";
			this.ultraLabel1.Size=new System.Drawing.Size(100, 23);
			this.ultraLabel1.TabIndex=0;
			this.ultraLabel1.Text="ultraLabel1";
			// 
			// ultraGroupBox1
			// 
			this.ultraGroupBox1.Location=new System.Drawing.Point(44, 103);
			this.ultraGroupBox1.Name="ultraGroupBox1";
			this.ultraGroupBox1.Size=new System.Drawing.Size(200, 110);
			this.ultraGroupBox1.TabIndex=1;
			this.ultraGroupBox1.Text="ultraGroupBox1";
			// 
			// ultraTextEditor1
			// 
			this.ultraTextEditor1.Location=new System.Drawing.Point(89, 41);
			this.ultraTextEditor1.Name="ultraTextEditor1";
			this.ultraTextEditor1.Size=new System.Drawing.Size(100, 21);
			this.ultraTextEditor1.TabIndex=2;
			this.ultraTextEditor1.Text="ultraTextEditor1";
			// 
			// button1
			// 
			this.button1.Location=new System.Drawing.Point(187, 12);
			this.button1.Name="button1";
			this.button1.Size=new System.Drawing.Size(75, 23);
			this.button1.TabIndex=3;
			this.button1.Text="button1";
			this.button1.UseVisualStyleBackColor=true;
			// 
			// label1
			// 
			this.label1.AutoSize=true;
			this.label1.Location=new System.Drawing.Point(131, 248);
			this.label1.Name="label1";
			this.label1.Size=new System.Drawing.Size(35, 13);
			this.label1.TabIndex=4;
			this.label1.Text="label1";
			// 
			// Form1
			// 
			this.AutoScaleDimensions=new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode=System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize=new System.Drawing.Size(292, 273);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.ultraTextEditor1);
			this.Controls.Add(this.ultraGroupBox1);
			this.Controls.Add(this.ultraLabel1);
			this.Name="Form1";
			this.Text="Form1";
			this.Load+=new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit( );
			((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit( );
			this.ResumeLayout(false);
			this.PerformLayout( );

		}

		#endregion

		private Infragistics.Win.Misc.UltraLabel ultraLabel1;
		private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
	}
}