﻿using Microsoft.FxCop.Sdk;


public abstract class BaseRule:BaseIntrospectionRule
{
	protected BaseRule(string name)
		: base(
			name,
			//typeof(BaseRule).Assembly.GetName().Name+".Rules",
			"GMS_Rules.RuleMetadata",
			typeof(BaseRule).Assembly
			) { }



}

