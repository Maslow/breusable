﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.FxCop.Sdk;
using Microsoft.VisualStudio.CodeAnalysis.Extensibility;

namespace GMS_Rules
{
	public class DoNotCallMessageBox:Supers.DoNotUseMethod
	{

		public DoNotCallMessageBox( )
			: base("DoNotCallMessageBox", "System.Windows.Forms.MessageBox.S") { }

	}
}