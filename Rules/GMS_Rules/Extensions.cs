﻿using System;
using System.Collections.Generic;

namespace GMS_Rules
{
	public static class Extensions
	{
		public static IEnumerable<T> WhileEnumerating<T>(this IEnumerable<T> items, Action<T> action)
		{
			foreach (var item in items)
			{
				action(item);
				yield return item;
			}
		}
		static public IEnumerable<T> Descendants<T>(this IEnumerable<T> source,
												Func<T, IEnumerable<T>> descendBy)
		{
			foreach (T value in source)
			{
				yield return value;

				foreach (T child in descendBy(value).Descendants(descendBy))
				{
					yield return child;
				}
			}
		}
	}
}
