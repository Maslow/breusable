﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.FxCop.Sdk;

public class DoNotCallGcAddMemoryPressure:GMS_Rules.Supers.DoNotUseMethod
{
	public DoNotCallGcAddMemoryPressure( ) : base("DoNotCallGcAddMemoryPressure", "System.GC.AddMemoryPressure") { }


}

