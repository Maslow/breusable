﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.FxCop.Sdk;

namespace GMS_Rules.Supers
{
	public abstract class DoNotUseMethod:BaseRule
	{
		protected readonly string MethodFullName;
		protected DoNotUseMethod(string name, string methodFullName) : base(name)
		{
			MethodFullName = methodFullName;
		}

		public override ProblemCollection Check(TypeNode type)
		{
			if (type.Namespace.Name.Contains("UnitTest"))
				return null;
			var methods = type.Members.Where(x => x is Method).Cast<Method>( );
			foreach (var method in methods)
			{
				VisitMethod(method);
			}
			return Problems;
		}

		public override void VisitMethod(Method method)
		{
			foreach (Instruction instruction in method.Instructions)
			{

				if (instruction.OpCode==OpCode.Call&&instruction.Value.ToString( ).StartsWith(MethodFullName))
					Problems.Add(new Problem(this.GetResolution(instruction.Value.ToString( )), instruction));
			}
			//	VisitStatements(method.Body.Statements);

			base.VisitMethod(method);
		}
	}
}
