﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.FxCop.Sdk;

namespace GMS_Rules
{
	/// <summary>
	/// http://stackoverflow.com/questions/410719/notimplementedexception-are-they-kidding-me/410800#410800
	/// </summary>
	public class DoNotRaiseNotImplementedException:BaseRule
	{
		private TypeNode _notImplementedException;
		private Member _currentMember;

		public DoNotRaiseNotImplementedException( ) : base("DoNotRaiseNotImplementedException") { }
		public override void BeforeAnalysis( )
		{
			base.BeforeAnalysis( );
			_notImplementedException=FrameworkAssemblies.Mscorlib.GetType(
				Identifier.For("System"),
				Identifier.For("NotImplementedException"));
		}
		public override ProblemCollection Check(Member member)
		{
			var method = member as Method;
			if (method!=null)
			{
				_currentMember=member;
				VisitStatements(method.Body.Statements);
			}
			return Problems;
		}
		public override void VisitThrow(ThrowNode throwInstruction)
		{
			if (throwInstruction.Expression!=null&&
				throwInstruction.Expression.Type.IsAssignableTo(_notImplementedException))
			{
				var problem = new Problem(
					GetResolution( ),
					throwInstruction.SourceContext,
					_currentMember.Name.Name);
				Problems.Add(problem);
			}
		}
	}
}
