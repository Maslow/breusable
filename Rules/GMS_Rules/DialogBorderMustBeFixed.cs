﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.FxCop.Sdk;

public class DialogBorderMustBeFixed:BaseRule
{
	public DialogBorderMustBeFixed( ) : base("DialogBorderMustBeFixed") { }

	private System.Windows.Forms.FormBorderStyle? borderStyle;

	public override ProblemCollection Check(TypeNode type)
	{
		borderStyle=null;
		if (type.Namespace.Name.Contains("UnitTest"))
			return null;
		if (type.Name.Name.EndsWith("Control"))
			return null;
		var initalizers = type.Members.Where(x => x is Method&&x.Name.Name=="InitializeComponent").OfType<Method>( );
		//Debug.WriteLine(initalizers.Count( ));
		if (initalizers.Any( )==false)
			return null;

		foreach (var member in initalizers)
			VisitMethod(member);
		if (borderStyle.HasValue==false || borderStyle.Value!= System.Windows.Forms.FormBorderStyle.FixedDialog)
			Problems.Add(new Problem(this.GetResolution(borderStyle.HasValue?borderStyle.Value.ToString():"not set"),type));
		return Problems;
	}

	public override void VisitMethod(Method method)
	{
		base.VisitMethod(method);
		Instruction prevInstruction=null;
		foreach (var item in method.Instructions)
		{
			
			//if(item.OpCode== OpCode.Ldc_I4)
			
			if (prevInstruction!=null&&item.OpCode==OpCode.Call&&item.Value!=null && item.Value.ToString( ).Contains("set_FormBorderStyle")&&item.Value is Method)
			{
				var instructionMethod=item.Value as Method;
				if (instructionMethod.FullName=="System.Windows.Forms.Form.set_FormBorderStyle(System.Windows.Forms.FormBorderStyle)")
				{
					var value=(System.Windows.Forms.FormBorderStyle) prevInstruction.Value;
						borderStyle=value;
						return;
				}


			}
			if (item.OpCode!=OpCode.Nop)
				prevInstruction=item;
		}

	}

	//private void FindBorderStyle(TypeNode type)
	//{
	//    var methods = type.Members.OfType<Method>();
	//    var formBorderStyleInstructions=methods.Select(x => x.Instructions.Where(i => i.Value.ToString().Contains("System.Windows.Forms.FormBorderStyle")));
	//    foreach (var item in formBorderStyleInstructions)
	//    {
	//        Debug.WriteLine(item);
	//    }

	//}
}

