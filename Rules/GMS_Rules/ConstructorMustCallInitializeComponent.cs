﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.FxCop.Sdk;

namespace GMS_Rules
{
	public class ConstructorMustCallInitializeComponent:BaseRule
	{
		public ConstructorMustCallInitializeComponent( ) : base("ConstructorMustCallInitializeComponent") { }



		public override ProblemCollection Check(TypeNode type)
		{
			Debug.WriteLine("Checking type:"+type.FullName);
			var initializer = type.Members.OfType<Method>( ).FirstOrDefault(x => x.FullName==type.FullName+".InitializeComponent");

			if (initializer==null)
				return null;
			Debug.WriteLine(initializer.FullName);
			var constructorsWithNoInitCall = type.Members.OfType<Method>( ).Where(m => m.NodeType==NodeType.InstanceInitializer).ToList( );
			var visitedMethods = new HashSet<string>( );
			var foundMethods = new ObservableHashSet<Method>( );

			var whenMethodsFound = Observable.FromEvent<NotifyCollectionChangedEventArgs>(foundMethods, "CollectionChanged");

			whenMethodsFound.Subscribe(
				e =>
				{
					switch (e.EventArgs.Action)
					{
						case NotifyCollectionChangedAction.Add:
							if (constructorsWithNoInitCall.Any( ))
								Parallel.ForEach(e.EventArgs.NewItems.Cast<Method>( ).Where(m => visitedMethods.Any(v => v==m.FullName)==false),
									i =>
									{

										lock (visitedMethods)
										{
											if (visitedMethods.Contains(i.FullName))
												return;
											visitedMethods.Add(i.FullName);

										}
										Debug.WriteLine("Visiting:"+i.FullName);
										var callers = (CallGraph.CallersFor(i));
										constructorsWithNoInitCall.RemoveAll(x => callers.Any(c => x.FullName==c.FullName));
										if (constructorsWithNoInitCall.Any( ))
											foreach (var item in callers.Where(c => visitedMethods.Any(v => v==c.FullName)==false))
											{
												foundMethods.Add(item);
											}
									});
							break;
						default:
							break;
					}
				}
				);
			foundMethods.Add(initializer);


			ReportProblem(constructorsWithNoInitCall, type);

			return Problems;
		}
		private void ReportProblem(IEnumerable<Method> badConstructorMethods, TypeNode type)
		{
			//sourceContext is wrong
			foreach (var constructor in badConstructorMethods)
			{



				Func<Instruction, string, bool> compareFullName =
					(i, fullName) => ((InstanceInitializer)i.Value).FullName.StartsWith(fullName+".#ctor");

				var source2 = constructor.Instructions.Where(i => i.Value is InstanceInitializer&&i.SourceContext.FileName!=null)
					.FirstOrDefault(i => compareFullName(i, type.FullName)
					||compareFullName(i, type.BaseType.FullName));

				Problems.Add(new Problem(this.GetResolution(constructor.FullName), source2!=null?source2.SourceContext:type.SourceContext));
			}

		}
		/*
		public override ProblemCollection Check(TypeNode type)
		{
			var initalizers = type.Members.Where(x => x is Method&&x.Name.Name=="InitializeComponent").OfType<Method>( );
			//Debug.WriteLine(initalizers.Count( ));
			if (initalizers.Any( )==false)
				return null;
			_methodContainsInitCall=new Dictionary<Method, bool>( );
			_constructorFoundInitializeCall=new Dictionary<Method, bool>( );

			foreach (var member in type.Members.OfType<Method>( ).Where(m => m.NodeType==NodeType.InstanceInitializer))
			{
				_constructorFoundInitializeCall.Add(member, false);
				_methodContainsInitCall.Add(member, false);
				VisitConstructorsRecursive(member, member);

			}


			foreach (var constructor in _constructorFoundInitializeCall.Keys)
			{
				if (_constructorFoundInitializeCall[constructor])
					continue;
				//sourceContext is wrong


				Func<Instruction, string, bool> compareFullName =
					(i, fullName) => ((InstanceInitializer)i.Value).FullName.StartsWith(fullName+".#ctor");

				var source2 = constructor.Instructions.Where(i => i.Value is InstanceInitializer&&i.SourceContext.FileName!=null)
					.FirstOrDefault(i => compareFullName(i, type.FullName)
					||compareFullName(i, type.BaseType.FullName));
#if DEBUG

				foreach (var i in constructor.Instructions)
				{
					Debug.WriteLine(i.SourceContext.FileName+" "+i.SourceContext.StartLine);
				}
#endif
				Problems.Add(new Problem(this.GetResolution(constructor.FullName), source2!=null?source2.SourceContext:type.SourceContext));
			}
			return Problems;
		}
		*/


		//public void VisitConstructorsRecursive(Method method, Method initializer)
		//{
		//    Debug.Assert(_methodContainsInitCall.ContainsKey(method));
		//    var toVisit = new List<Method>( );
		//    foreach (var instruction in method.Instructions.Where(x => x.OpCode==OpCode.Call))
		//    {

		//        if (instruction.Value is Method)
		//        {
		//            //&&((Method)instruction.Value).FullName.Contains(".#ctor")
		//            var callMethod =(Method)instruction.Value;
		//            if (callMethod.FullName.StartsWith("System.Windows.Forms.Form.#ctor"))
		//                continue;
		//            if (callMethod.IsStatic==false) //can not call instance method InitializeComponent in static method
		//            {
		//                toVisit.Add(callMethod);
		//            }

		//            //
		//            //TestLibrary.Form1.#ctor(System.String)
		//        }
		//        if (instruction.Value is Method&&((Method)instruction.Value).FullName.EndsWith(".InitializeComponent"))
		//        {
		//            if (_constructorFoundInitializeCall.ContainsKey(method))
		//                _constructorFoundInitializeCall[method]=true;
		//            _methodContainsInitCall[method]=true;
		//            return;
		//        }

		//    }
		//    foreach (var methodCall in toVisit)
		//    {
		//        if (_methodContainsInitCall.ContainsKey(methodCall))
		//        {
		//            if (_methodContainsInitCall[methodCall])
		//            {
		//                _constructorFoundInitializeCall[initializer]=true;
		//                return;
		//            }
		//        }
		//        else
		//        {
		//            _methodContainsInitCall.Add(methodCall, false);
		//            VisitConstructorsRecursive(methodCall, initializer);
		//        }


		//    }
		//}
		//        public override void VisitMembers(MemberCollection members)
		//        {

		//            foreach (var member in members.OfType<Method>( ).Where(m => m.NodeType==NodeType.InstanceInitializer))
		//            {
		//                if (_callers.Contains(member))
		//                    continue;

		//                Func<Instruction, string, bool> compareFullName =
		//                    (i, fullName) => ((InstanceInitializer)i.Value).FullName.StartsWith(fullName+".#ctor");

		//                var source2 = member.Instructions.Where(i => i.Value is InstanceInitializer&&i.SourceContext.FileName!=null)
		//                    .FirstOrDefault(i => compareFullName(i, type.FullName)
		//                    ||compareFullName(i, type.BaseType.FullName));
		//#if DEBUG

		//                foreach (var i in member.Instructions)
		//                {
		//                    Debug.WriteLine(i.SourceContext.FileName+" "+i.SourceContext.StartLine);
		//                }
		//#endif
		//                Problems.Add(new Problem(this.GetResolution(member.FullName), source2!=null?source2.SourceContext:type.SourceContext));

		//            }
		//            base.VisitMembers(members);
		//        }
		//        public override void VisitMethodCall(MethodCall call)
		//        {
		//            if (call.Callee==null)
		//                Debug.WriteLine(call.UniqueKey);
		//            else
		//                Debug.WriteLine(call.Callee.SourceContext.FileName+" "+call.Callee.SourceContext.StartLine);

		//            base.VisitMethodCall(call);
		//        }
		//        public override void VisitMemberBinding(MemberBinding memberBinding)
		//        {
		//            Debug.WriteLine("Memberbinding");
		//            base.VisitMemberBinding(memberBinding);
		//        }
	}
}
