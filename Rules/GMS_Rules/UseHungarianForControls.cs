﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.FxCop.Sdk;

namespace GMS_Rules
{
	public class UseHungarianForControls:BaseRule
	{
		public UseHungarianForControls( ) : base("UseHungarianForControls") { }

		public override ProblemCollection Check(Member member)
		{

			if (member is Field||member is PropertyNode)
				Visit(member);

			return Problems;


		}
		private static bool RuleApplies(TypeNode member)
		{


			return member.FullName.StartsWith("Infragistics")||(member.BaseType!=null&&(
				   member.BaseType.FullName=="System.Windows.Forms.Control"
				   ||member.BaseType.FullName=="System.Windows.Forms.UserControl"));
		}
		public override void VisitField(Field field)
		{

			if (RuleApplies(field.Type)&&_PrefixMap.ContainsKey(field.Type.Name.Name))
			{
				var mapPrefix = _PrefixMap[field.Type.Name.Name];
				var fieldName = field.Name.Name;
				if (fieldName.StartsWith(mapPrefix)==false)
					Problems.Add(new Problem(GetResolution(fieldName, mapPrefix), field));
			}
			base.VisitField(field);
		}

		private static readonly IDictionary<string, string> _PrefixMap = 
			new Dictionary<string, string>( )
				{
					{"UltraButton", "btn"},{"Button","btn"},
					{"UltraLabel", "lbl"},{"Label","lbl"},
					{"UltraGroupBox","grp"},{"TextBox","txt"},
					{"UltraTextEditor","txt"}
				};

	}
}
