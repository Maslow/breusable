﻿using Microsoft.FxCop.Sdk;
namespace GMS_Rules
{
	public class FieldsShouldBePrivate:BaseRule
	{
		public FieldsShouldBePrivate( ) : base("FieldsShouldBePrivate") { }

		public override ProblemCollection Check(Member member)
		{
			if (member is Field&&member.IsPrivate==false)
				Problems.Add(new Problem(this.GetResolution(member.Name.Name), member));
			return Problems;
		}
	}
}
