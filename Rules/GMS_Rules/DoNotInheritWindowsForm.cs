﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.FxCop.Sdk;


	public class DoNotInheritWindowsForm:BaseRule
	{
		public DoNotInheritWindowsForm( )
			: base("DoNotInheritWindowsForm")
		{
			//Debug.WriteLine("RuleInitialized:" + typeof(DoNotInheritWindowsForm).Name);
		}
		
		public override ProblemCollection Check(TypeNode type)
		{
			if (type.Namespace.Name.Contains("UnitTest"))
				return null;
			if (type.BaseType.FullName=="System.Windows.Forms.Form")
				Problems.Add(new Problem(this.GetResolution(type.BaseType.Name.Name), type));
			return Problems;
		}
	}

