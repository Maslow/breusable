﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace GMS_Rules
{
	public class ObservableHashSet<T>:ObservableCollection<T>
	{
		public const string SetItemAlreadyExists = "Set item already exists";

		public Func<T, Exception> DuplicateExceptionGenerator { get; set; }
		public bool ThrowOnDuplicateInsertion { get; set; }

		public ObservableHashSet( ) { }
		public ObservableHashSet(bool throwOnDuplicateInsertion=false, Func<T, Exception> duplicateExceptionGenerator=null)
		{
			ThrowOnDuplicateInsertion=throwOnDuplicateInsertion;
			DuplicateExceptionGenerator=duplicateExceptionGenerator;
		}

		/// <summary>
		/// Will enumerate the list, to check for uniqueness
		/// </summary>
		/// <param name="items"></param>
		/// <param name="throwOnDuplicateInsertion">throw on duplicate, or just continue</param>
		/// <param name="duplicateExceptionGenerator"></param>
		public ObservableHashSet(IEnumerable<T> items,
			bool throwOnDuplicateInsertion=false, Func<T, Exception> duplicateExceptionGenerator=null)
			: this(throwOnDuplicateInsertion, duplicateExceptionGenerator)
		{

			foreach (var item in items)
			{
				Add(item);
			}
		}

		/// <summary>
		/// Optimization for using the list's count property to buffer out the needed size ahead of time
		/// </summary>
		/// <param name="items"></param>
		/// <param name="throwOnDuplicateInsertion"></param>
		/// <param name="duplicateExceptionGenerator"></param>
		public ObservableHashSet(List<T> items, bool throwOnDuplicateInsertion=false, Func<T, Exception> duplicateExceptionGenerator=null)
			: base((items!=null)?new List<T>(items.Count):items)
		{
			ThrowOnDuplicateInsertion=throwOnDuplicateInsertion;
			DuplicateExceptionGenerator=duplicateExceptionGenerator;
			foreach (var item in items)
			{
				Add(item);
			}
		}


		protected override void InsertItem(int index, T item)
		{
			if (Contains(item))
			{

				if (ThrowOnDuplicateInsertion)
					throw (DuplicateExceptionGenerator!=null
							?DuplicateExceptionGenerator(item):new InvalidOperationException(SetItemAlreadyExists));
			}
			else
				base.InsertItem(index, item);
		}
		protected override void SetItem(int index, T item)
		{
			int i = IndexOf(item);
			if (i>=0&&i!=index)
			{
				if (ThrowOnDuplicateInsertion)
					throw (DuplicateExceptionGenerator!=null
							?DuplicateExceptionGenerator(item):new InvalidOperationException(SetItemAlreadyExists));
			}
			else
				base.SetItem(index, item);
		}
	}
}
