﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using BReusable;


	public static class BufferedLinqEntityExtensions
	{
		/// <summary>
		/// Code to attach to a bindingnavigator, and ask the user to save changes or not.
		/// </summary>
		/// <param name="form">used to make a call to Form.Validate
		/// in case the navigation is clicked while a control has not validated yet</param>
		/// <param name="bn">the bindingNavigator that is tied to your item</param>
		/// <param name="hasErrorsFunc">a call to a function that validates the current data showing, also called after a successful move
		/// ignored if null</param>
		/// <param name="onMoveSuccess">What to do if the move is called usually something like bn.bindingsource.movenext, etc..</param>
		/// <param name="onSaveRequest">usually a call to bn.current.directcast.endEdit()</param>
		public static DialogResult ConditionalMove<TContext,TEntity>(Form formToValidate,  BindingNavigator bn
			, Func<bool> hasErrorsFunc, Action onMoveSuccess, Action onSaveRequest) 
			where TContext:System.Data.Linq.DataContext
			where TEntity:class
		{
			Debug.Assert(bn.BindingSource != null);
			Debug.Assert(bn.BindingSource.Current != null);
			formToValidate.Validate();

			Action moveSuccess = () =>
			{

				onMoveSuccess();
				hasErrorsFunc();
				//    itemChanges = false; 

			};

			var current = bn.BindingSource.Current.DirectCast<BufferedLinqEntity2<TContext, TEntity>>();

			bool hasErrors = false;
			if (hasErrorsFunc != null)
				hasErrors = hasErrorsFunc();
			else
				Debug.Print("hasErrorsFunc is null, ignoring");
			var hasChanges = current != null ? current.HasChanges : false;
			String prompt;
			prompt = hasErrors ? "Cannot save because of errors,fix errors?" : "Save changes?";

			if (hasErrors || hasChanges) //|| itemChanges)

				switch (MessageBox .Show("Save changes?","Save changes", MessageBoxButtons.YesNoCancel))
				{
					case DialogResult.Yes:
						if (hasErrors)
							return DialogResult.Cancel;
						try
						{
							onSaveRequest();
						}

						catch (Exception exception)
						{
							exception.Message.ToMessageBox();
							return DialogResult.Cancel;
						}

						moveSuccess();
						return DialogResult.Yes;

					case  DialogResult.No:
						bn.BindingSource.CancelEdit();
						current.ThrowIfNull().CancelEdit();
						moveSuccess();
						return DialogResult.No;


					case DialogResult.Cancel:
						return DialogResult.Cancel;
					//does nothing user wishes to stay and work on the current record

					default:
						throw new NotImplementedException();


				}

			moveSuccess();
			return DialogResult.None;
		}
	}

