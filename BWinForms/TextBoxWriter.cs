﻿using System;
using System.Text;
using System.Windows.Forms;

namespace BWinForms
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>http://kasperbirch.wordpress.com/2007/12/12/redirecting-datacontextlog-to-textbox/</remarks>
    public class TextBoxWriter : System.IO.TextWriter
    {
        private Encoding _encoding;
        private readonly TextBox _textBox;
		

        public TextBoxWriter(TextBox textBox)
        {
            if (textBox == null)
                throw new NullReferenceException();
            _textBox = textBox;
        }
        public override Encoding Encoding
        {
            get
            {
                if (_encoding == null)
                {
                    _encoding = new UnicodeEncoding(false, false);
                }
                return _encoding;
            }
        }
        public override void Write(string value)
        {
            _textBox.AppendText(value);
        }

        public override void Write(char[] buffer)
        {
            Write(new string(buffer));
        }
        public override void Write(char[] buffer, int index, int count)
        {
            Write(new string(buffer, index, count));
        }
    }
}