﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

public static	class WinFormStringExtensions
	{

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="text"></param>
	/// <param name="caption"></param>
	/// <returns></returns>
	public static bool IfNoDebuggerOrPromptYes(this string text, string caption)
	{
		return Debugger.IsAttached == false || MessageBox.Show(text, caption, MessageBoxButtons.YesNo) == DialogResult.Yes;
	}

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="text"></param>
	/// <returns></returns>
	public static String ToMessageBox(this String text)
	{
		MessageBox.Show(text);
		return text;
	}

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="text"></param>
	/// <param name="titleText"></param>
	/// <param name="buttons"></param>
	/// <returns></returns>
	public static DialogResult ToMessageBox(this String text, String titleText, MessageBoxButtons buttons)
	{
		return MessageBox.Show(text, titleText, buttons);
	}
	}
