﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BReusable.StaticReflection;

namespace BReusable.Tests.StaticReflection
{
	[TestClass]
	public class MaybeTests
	{

		[TestMethod]
		public void Maybe_nullValue_ReturnsNull()
		{
			//arrange
			var instance = new StaticReflectionTestHelper();
			var actual = instance.Maybe(x => x.InstanceNullableIntFieldWithValue);
			var expected = StaticReflectionTestHelper.HasValueConstant;
			Assert.AreEqual(expected, actual);
		}

		[TestMethod]
		public void Maybe_PrimitiveValue_ThrowsArgumentException()
		{
			var instance = new StaticReflectionTestHelper();
			var expected = MaybeWalker.MaybePrimitiveArgumentException();
			try
			{
				var action = instance.Maybe(x => x.InstanceIntField);
			}
			catch (Exception ex)
			{
				Assert.IsInstanceOfType(ex, typeof(ArgumentException));
			}

		}

		[TestMethod]
		public void Maybe_NullableValueWithNull_ReturnsNull()
		{
			var instance = new StaticReflectionTestHelper();
			int? expected = null;
			var actual = instance.Maybe(x => x.InstanceNullableIntField);
			Assert.AreEqual(expected, actual);
		}
		[TestMethod]
		public void Maybe_NullableValueWithValue_ReturnsValue()
		{
			var instance = new StaticReflectionTestHelper();
			int? expected = StaticReflectionTestHelper.HasValueConstant;
			var actual = instance.Maybe(x => x.InstanceNullableIntFieldWithValue);
			Assert.AreEqual(expected, actual);
		}

	}
}
