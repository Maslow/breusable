﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BReusable.StaticReflection;

namespace BReusable.Tests.StaticReflection
{
	[TestClass]
	public class MemberTests
	{

		[TestMethod]
		public void ValueName_InstancedDeepLambdaProperty_ReturnsOnlyTheLastAccessName()
		{
			var x = new { y = new { z = "DeepValue" } };
			var expected = "z";

			var actual = Member.ValueName(x, exp => exp.y.z);

			Assert.AreEqual(expected, actual);
		}
		[TestMethod]
		public void ValueBindingName_InstancedDeepLambdaProperty_ReturnsCallChain()
		{
			var x = new { y = new { z = "DeepValue" } };
			var expected = "y.z";
			var actual = Member.ValueBindingName(x, exp => exp.y.z);

			Assert.AreEqual(expected, actual);
		}
		[TestMethod]
		public void ValueName_StaticDeepLambdaProperty_ReturnsOnlyTheLastAccessName()
		{
			var expected = "DeepProperty";
			var actual = Member.StaticValueName(() => StaticReflectionTestHelper.StaticDeep.DeepProperty);
			Assert.AreEqual(expected, actual);
		}
		[TestMethod]
		public void ValueBindingName_StaticDeepLambdaProperty_ReturnsCallChain()
		{
			var expected = "NestedInstance.DeepProperty";
			var actual = Member.ValueBindingName(StaticReflectionTestHelper.StaticDeep, v => v.NestedInstance.DeepProperty);

			Assert.AreEqual(expected, actual);
		}
		[TestMethod]
		public void ValueName_InstanceFunction_ReturnsFunctionName()
		{
			var expected = "ToString";
			var actual = string.Empty.ValueName(t => t.ToString());

			Assert.AreEqual(expected, actual);
		}
		[TestMethod]
		public void ValueName_StaticField_ReturnsFieldName()
		{
			var expected = "Empty";
			var actual = Member.StaticValueName(() => string.Empty);
			Assert.AreEqual(expected, actual);


		}

		[TestMethod]
		public void StaticMethodName_StaticFunction_ReturnsFunctionName()
		{
			var expected = "Concat";
			var actual = Member.StaticMethodName(() => string.Concat("test1", "test2"));
			Assert.AreEqual(expected, actual);
		}

		[TestMethod]
		public void ValueName_Constant_ReturnsConstantName()
		{

			var expected = "STATICSCONSTANT";
			string actual = null;
			try
			{
				actual = Member.StaticValueName(() => StaticReflectionTestHelper.STATICSCONSTANT);
			}
			catch (ArgumentException ex)
			{

				Assert.Fail(this.ClassMemberName(x => x.ValueName_Constant_ReturnsConstantName()) +
					"threw exception:" + ex.Message);
			}
			Assert.AreEqual(expected, actual);


		}

		[TestMethod]
		public void VariableName_LocalVariable_ReturnsLocalName()
		{
			bool variableName = false;
			var expected = "variableName";
			var actual = Member.VariableName(() => variableName);
			Assert.AreEqual(expected, actual);
		}
		[TestMethod]
		public void VariableName_ClassVariable_ReturnsVariableName()
		{
			var expected = "classLevelTestHelper";
			var actual = Member.VariableName(() => this.classLevelTestHelper);
			Assert.AreEqual(expected, actual);
		}

		private StaticReflectionTestHelper classLevelTestHelper = null;
	}
}
