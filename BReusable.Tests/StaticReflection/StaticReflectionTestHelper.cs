﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BReusable.Tests.StaticReflection
{
	class StaticReflectionTestHelper
	{

		
		#region Statics
		public const StaticReflectionTestHelper STATICSCONSTANT = null;
		public const int HasValueConstant = 1;
		public static string StaticField = null;
		public static string StaticProperty { get; set; }
		public static int StaticIntField = HasValueConstant;
		public static int? StaticNullableIntField = null;
		public static int? StaticNullableIntFieldWithValue = HasValueConstant;
		public static StaticReflectionTestHelper StaticDeep = null;

		#endregion


		public int InstanceIntField = HasValueConstant;

		public int? InstanceNullableIntField = null;
		public int? InstanceNullableIntFieldWithValue = HasValueConstant;
		
		
		public string DeepProperty { get; set; }
		public StaticReflectionTestHelper NestedInstance { get; set; }
	
		public static void StaticProcedure()
		{
		}
		public static string StaticFunction()
		{
			return string.Empty;
		}
	}
}
