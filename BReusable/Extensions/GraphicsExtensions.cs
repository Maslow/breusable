﻿using System.Drawing;


public static class GraphicsExtensions
{

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="bmp"></param>
	/// <returns></returns>
	public static Icon ToIcon(this Bitmap bmp)
	{
		return Icon.FromHandle(bmp.GetHicon());
	}

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="srcBmp"></param>
	/// <param name="fromRect"></param>
	/// <returns></returns>
	public static Bitmap CopyPart(this Bitmap srcBmp, Rectangle fromRect)
	{
		var toBmp = new Bitmap(fromRect.Width, fromRect.Height);

		using (var g = Graphics.FromImage(toBmp))
		{
			g.DrawImage(srcBmp, new Rectangle(0, 0, fromRect.Width, fromRect.Height), fromRect, GraphicsUnit.Pixel);
		}
		return toBmp;

	}

	public static Bitmap CopyPart(this Bitmap srcBmp, int topLeftX, int topLeftY, int bottomRightX, int bottomRightY)
	{
		return srcBmp.CopyPart(new Rectangle(topLeftX, topLeftY, bottomRightX - topLeftX, bottomRightY - topLeftY));
	}

	/// <summary>
	/// From BReusable
	/// </summary>
	/// <param name="srcBmp"></param>
	/// <param name="fromRect"></param>
	/// <returns></returns>
	public static Icon IconFromBitMapPart(this Bitmap srcBmp, Rectangle fromRect)
	{
		return srcBmp.CopyPart(fromRect).ToIcon();
	}
	public static Icon IconFromBitMapPart(this Bitmap srcBmp, int topLeftX, int topLeftY, int bottomRightX, int bottomRightY)
	{
		return srcBmp.CopyPart(topLeftX, topLeftY, bottomRightX, bottomRightY).ToIcon();
	}

}
