﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BReusable.Containers
{
	/// <summary>
	/// Shallow immutability
	/// </summary>
	/// <typeparam name="TKey"></typeparam>
	/// <typeparam name="TValue"></typeparam>
	public class ImmutableDictionary<TKey, TValue> : IEnumerable<TValue>
	{
		private readonly IDictionary<TKey, TValue> _dictionary;

		public ImmutableDictionary(IEnumerable<TValue> values, Func<TValue, TKey> getKeyFunc)
		{
			_dictionary = new LambdaDictionary<TKey, TValue>(getKeyFunc);
		}
		public ImmutableDictionary(IDictionary<TKey, TValue> dictionary)
		{
			_dictionary = dictionary;
		}

		public bool ContainsKey(TKey key)
		{
			return _dictionary.ContainsKey(key);
		}
		public bool TryGetValue(TKey key, out TValue value)
		{
			return _dictionary.TryGetValue(key, out value);
		}
		public ICollection<TKey> Keys
		{
			get { return _dictionary.Keys; }
		}
		public ICollection<TValue> Values
		{
			get
			{
				return _dictionary.Values;
			}
		}
		public TValue this[TKey key]
		{
			get
			{
				return _dictionary[key];
			}
		}
		public bool Contains(KeyValuePair<TKey, TValue> item)
		{
			return _dictionary.Contains(item);
		}

		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			_dictionary.CopyTo(array, arrayIndex);
		}

		public int Count
		{
			get { return _dictionary.Count; }
		}

		public bool IsReadOnly
		{
			get { return true; }
		}

		#region IEnumerable<TValue> Members

		public IEnumerator<TValue> GetEnumerator()
		{
			return _dictionary.Values.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return _dictionary.Values.GetEnumerator();
		}

		#endregion
	}
}
