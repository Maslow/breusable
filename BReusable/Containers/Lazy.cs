﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BReusable.Containers
{
	public class Lazy<T> where T:class
	{
		readonly Func<T> _initializer;
		T _value;
		public T Value
		{
			get
			{
				if (_value == null)
				{
					_value = _initializer();
				}
				return _value;
			}
		}
		
		public Lazy(Func<T> initializer)
		{
			_initializer = initializer;
		}
	}
}
