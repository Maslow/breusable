﻿using System;

namespace BReusable
{
	[Obsolete("possible bugs with string")]
	public class BResult<T>
	{
		public ErrorString ErrorMessage { get; private set; }
		public T Result { get; private set; }
	public	BResult(ErrorString errorMessage)
		{
			ErrorMessage = errorMessage;
		}
	public BResult(T value)
	{
		Result = value;
	}
	}

	public class TryAction
	{
		public Exception Exception { get; protected set; }
		protected TryAction() { }
		public TryAction(Action actionToTry)
		{
			try
			{
				actionToTry();
			}
			catch (Exception exception)
			{
				Exception = exception;
			}
		}
	}
	public class TryAction<TException> where TException : Exception
	{
		public TryAction(Action actionToTry)
		{
			try
			{
				actionToTry();
			}
			catch (TException exception)
			{

				Exception = exception;
			}
		}
		protected TryAction() { }
		public  TException Exception { get; private set; }
	}

	public class TryFunc<T> :TryAction
	{
		public T Result { get; protected set; }
		public bool HasValue { get; protected set; }

		protected TryFunc() { }
		public TryFunc(Func<T> actionToTry)
		{
			try
			{
				Result = actionToTry();
				HasValue = true;
			}
			catch (Exception exception)
			{
				HasValue = false;
				Exception = exception;
			}
		}
	
		
		
	}
	public class TryFunc<T, TException> : TryFunc<T> where TException : Exception
	{
		public TryFunc(Func<T> actionToTry)   
		{
			try
			{
				Result = actionToTry();
			}
			catch (TException exception)
			{
				HasValue = false;
				Exception = exception;
			}

		}
		public new TException  Exception { get; private set; }

	}
}
