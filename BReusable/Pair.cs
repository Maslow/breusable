﻿using System;

namespace BReusable
{



	/// <summary>
	/// immutable tuple for two
	/// </summary>
	public class Pair<TValue1, TValue2> : Singleton<TValue1>
	{

		public TValue2 Value2 { get; private set; }


		public Pair(TValue1 value1, TValue2 value2)
			: this(value1, value2, (Func<String>)null) { }

		public Pair(TValue1 value1, TValue2 value2, Func<String> toStringFunc)
			: base(value1, toStringFunc)
		{
			Value2 = value2;
		}
		public Pair(TValue1 value1, TValue2 value2, Func<TValue1, TValue2, String> toStringFunc)
			: this(value1, value2,()=>toStringFunc(value1,value2)) { }
        
	}

}
