using System;
using BReusable;

public static class XmlFontExtensions
{
    /// <summary>
    /// From BReusable
    /// </summary>
    /// <param name="font"></param>
    /// <returns></returns>
    public static XmlFont ToXmlFont(this System.Drawing.Font font)
    {
        return new XmlFont(font);
    }   
}

namespace BReusable
{
	[Serializable]
public struct XmlFont
{
	public string FontFamily;
	public System.Drawing.GraphicsUnit GraphicsUnit;
	public float Size;
	public System.Drawing.FontStyle Style;

	public XmlFont(System.Drawing.Font f)
	{
		FontFamily = f.FontFamily.Name;
		GraphicsUnit = f.Unit;
		Size = f.Size;
		Style = f.Style;
	}

	public System.Drawing.Font ToFont()
	{
		return new System.Drawing.Font(FontFamily, Size, Style,
			GraphicsUnit);
	}
}
}

