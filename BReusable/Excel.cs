﻿using System;
using System.Data;

namespace BReusable
{


	static class Excel
	{

		public static DataTable GetSheets(System.Data.Odbc.OdbcConnection excelConnection)
		{
			return excelConnection.GetSchema(System.Data.Odbc.OdbcMetaDataCollectionNames.Tables);
		}

		public static DataTable GetSheets(System.Data.OleDb.OleDbConnection excelConnection)
		{
			return excelConnection.GetSchema(System.Data.OleDb.OleDbMetaDataCollectionNames.Tables);
		}

		/// <summary>
		/// using an integer get thecolumn string
		/// if startAt1 column 27 = "AA"
		/// if !startAt1 column 26="AA"
		/// returns null on bad column integer such as -1
		/// </summary>
		/// <param name="column"></param>
		/// <param name="startAt1">if false 0= column 'A'
		/// if true 1=column 'A'</param>
		/// <returns>null if column number is bad, column string if successfull</returns>
		public static String ExcelColumn(int column,bool startAt1)
		{
			if (startAt1) column--;

			if (column >= 0 && column < 26)
				return ((char)('A' + column)).ToString();
			if (column > 25)
				return ExcelColumn((int)(Math.Floor((float)column / 26)- (startAt1? 0: 1)), startAt1)
				       + ExcelColumn((column % 26) + (startAt1 ? 1 : 0), startAt1);
			return null;
		}
	
		

	}
}
