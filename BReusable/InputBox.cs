﻿using System;
using System.Windows.Forms;

namespace BReusable
{
	public partial class InputBox : Form
	{
		public static String ShowMessageBox()
		{
			using (var inputBox = new InputBox())
			{
				if (inputBox.ShowDialog() == DialogResult.OK)
					return inputBox.Value;
				return null;
			}
		}
		internal InputBox()
		{
			InitializeComponent();
		}
		public String Value { get { return _txtInput.Text; } set { _txtInput.Text = value; } }


		private void _btnOk_Click(object sender, EventArgs e)
		{
			if (_txtInput.Text.IsNullOrEmpty() == false)
			{
				DialogResult = DialogResult.OK;
				Close();
			}

		}

		private void _btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}
		
	}
}
